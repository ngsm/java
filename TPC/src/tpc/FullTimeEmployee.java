/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;

/**
 *
 * @author ngsm
 */
public class FullTimeEmployee extends Employee{
    
    private double monthlySalary;
    
    public FullTimeEmployee()
    {
        this("unknown", 0.0);
    }
    
    public FullTimeEmployee(String name, double salary)
    {
        super(name);
        setMonthlySalary(salary);
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public void setMonthlySalary(double monthlySalary) {
        if (monthlySalary >= 0)
            this.monthlySalary = monthlySalary;
    }
    
    @Override
    public String toString()
    {
        return getName() + " is Full Timer : Emp Num: " + getEmpNum()   + " and salary of " + monthlySalary;
    }

    @Override
    public double calculatePay() {
        return monthlySalary;
    }

    

   

   
}
