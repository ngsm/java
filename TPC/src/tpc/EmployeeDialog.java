/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class EmployeeDialog extends JDialog implements ActionListener
{
	private JLabel nameLabel, payLabel;
	private JTextField nameTF, payTF;
	private JRadioButton fullTimeRB, partTimeRB;
	private JButton okBtn, cancelBtn, resetBtn;
	
	private Employee employee;	// the employee that will be created
	
public EmployeeDialog(JFrame parent)
	{
		super(parent, true);	//creates a modal dialog
		employee = null;		// initialize employee
	
		nameLabel = new JLabel("Name: ");
		payLabel = new JLabel("Monthly Salary: ");
		nameTF = new JTextField(10);
		payTF = new JTextField(10);
		okBtn = new JButton("OK");
		resetBtn = new JButton("Reset");
		cancelBtn = new JButton("Cancel");
	
                // register the buttons for actionlistener
                okBtn.addActionListener(this);
                resetBtn.addActionListener(this);
                cancelBtn.addActionListener(this);
                
		ButtonGroup empType = new ButtonGroup();
		fullTimeRB = new JRadioButton("Full-Time");
		partTimeRB = new JRadioButton("Part-Time");
		fullTimeRB.setSelected(true);
		empType.add(fullTimeRB);
		empType.add(partTimeRB);
				
		JPanel p1 = new JPanel(new GridLayout(0,1,0,8));
		p1.add(nameLabel);
		p1.add(payLabel);
		
		JPanel p2 = new JPanel(new GridLayout(0,1,0,8));
		p2.add(nameTF);
		p2.add(payTF);
		
		JPanel p3 = new JPanel();
		p3.add(p1);
		p3.add(p2);
		
		JPanel p4 = new JPanel();
		p4.add(fullTimeRB);
		p4.add(partTimeRB);
		
		JPanel p5 = new JPanel();
		p5.add(okBtn);
		p5.add(resetBtn);
		p5.add(cancelBtn);
		
		JPanel p6 = new JPanel(new GridLayout(0,1));
		p6.add(p3);
		p6.add(p4);
		p6.add(p5);
		
		Container cp = getContentPane();
		cp.add(p6, BorderLayout.NORTH);
		
		setTitle("New Employee Entry");
		setSize(300,200);
		setLocation(80,200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
	}
	public Employee getEmployee()
	{
		return employee;
	}

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == okBtn)
        {
          try {  
            // get the text that was input
            String name = nameTF.getText().trim();
            String payStr = payTF.getText().trim();

          
            double pay = Double.parseDouble(payStr);
            // check whether full or part time
            if (fullTimeRB.isSelected())
                employee = new FullTimeEmployee(name, pay);
            else
                employee = new PartTimeEmployee(name, pay);

            setVisible(false);
          }
          catch (NumberFormatException ne){
              JOptionPane.showMessageDialog(this, "Please enter valid pay");
          }
        }
        else if (e.getSource() == resetBtn)
        {
            nameTF.setText("");
            payTF.setText("");
        }
        else if (e.getSource() == cancelBtn)
        {
             nameTF.setText("");
             payTF.setText("");
             employee = null;
             setVisible(false);
            
        }
    }
}
