/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;
import java.util.*; 
import java.io.Serializable;

/**
 * A class to represent <code>TPC</code> Controller
 * TPC has an array of Project objects
 * @author ngsm
 */
public class TPC implements Serializable {
    
    
    // TPC manages a collection of Projects
    private int numberOfProjects;
    private Project[] TPCProjects;
    private final int MAX = 50;

    // TPC manages a collection of Employees
    private ArrayList<Employee> TPCEmployees;
    
    // constructor
    public TPC()
    {
        numberOfProjects = 0;
        TPCProjects = new Project[MAX];
        
        TPCEmployees = new ArrayList<>();
    }

    public ArrayList<Employee> getEmployees() {
        return TPCEmployees;
    }

    public void setEmployees(ArrayList<Employee> TPCEmployees) {
        this.TPCEmployees = TPCEmployees;
    }
    
    
    /** 
     * add a project to TPC
     * @param projectName
     * @return Project
     */
    public Project addProject(String projectName)
    {
        if(numberOfProjects == TPCProjects.length)
            return null;
        Project newProj = new Project(projectName);
        TPCProjects[numberOfProjects++] = newProj;
        return newProj;
    }
    
    // Add Employee to TPC
    public boolean addEmployee(Employee e)
    {
        if (TPCEmployees.contains(e))
            return false;
        TPCEmployees.add(e);
        return true;
    }
    
    public Employee findEmployee(int ID)
    {
        for (int i = 0; i < TPCEmployees.size(); i++)
        {
            if (TPCEmployees.get(i).getEmpNum()==ID)
                return TPCEmployees.get(i);
                
        }
        return null;
    }
    
    // find Employee by name
    public Employee findEmployee(String name)
    {
        
        for (Employee e : TPCEmployees)
        {
            if (e.getName().equals(name))
                return e;
        }   
        return null;
    }
    
    /**
     * find project is a method to find a project based on the project number
     */
    public Project findProject(int projectNum)
    {
                   
           return TPCProjects[projectNum];
           
    }
    
    /**
     * find project based on project name
     */
    public String findProject(String name)
    {
        String foundProjects = "Found Projects containing " + name + ":\n";
        for (int i =0; i < numberOfProjects; i++)
        {
            Project p = TPCProjects[i];
            if (p.getProjectName().contains(name))
                foundProjects +=  p.toString()+ "\n";
        }
        return foundProjects;
    }
    
     public String showAllProjects()
    {
        String foundProjects = "All Projects\n";
        for (int i =0; i < numberOfProjects; i++)
        {
           foundProjects +=  TPCProjects[i].toString()+ "\n";
        }
        return foundProjects;
    }
     
     public double payAllEmployees()
     {
         double pay = 0.0;
         for (Employee e:TPCEmployees)
         {
             pay += e.calculatePay();
         }
         return pay;
     }
}
