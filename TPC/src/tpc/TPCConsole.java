/*
 * TPC is a project company and we need to manage the 
 * projects and employees
 */
package tpc;
import java.io.*;
import java.util.*;

/**
 *
 * @author ngsm
 */
public class TPCConsole {

    /**
     * @param args the command line arguments
     */
    static TPC tpc;
    static Scanner sc;
    
    public static void main(String[] args) {
       
        sc = new Scanner(System.in);
        tpc = new TPC();
        
        System.out.println("Welcome to The Project Company");
        loadFile();
        int choice;
        do
        {
            System.out.println("Would you like to :");
            System.out.println("1. Add a New Project");
            System.out.println("2. Find project");
            System.out.println("3. Show all projects");
            System.out.println("4. Add Task to Project");
            System.out.println("5. Add Employee");
            System.out.println("6. Show all Employees");
            System.out.println("7. Add Timesheet for Part Timer");
            System.out.println("8. Process Payroll");
            System.out.println("0. Save and Quit");
            System.out.print("Enter choice :");
            choice = sc.nextInt();
            sc.nextLine();
            switch(choice)
            {
                case 1: addNewProject(); break;
                case 2: findProject(); break;
                case 3: System.out.println(tpc.showAllProjects());break;
                case 4: addTaskToProject(); break;
                case 5: addEmployee(); break;
                case 6: showEmployees(); break;
                case 7: addTimesheetToEmployee();break;
                case 8: processPayroll(); break;
                case 0: saveAndQuit(); break;
                default: System.out.println("Error"); 
            }
            
        } while (choice != 0);
    }
    
    /** A method to add a task to a project 
     * 
     */
    public static void addTaskToProject()
    {
        System.out.print("Enter project number of project to find :");
        int numWanted = sc.nextInt();
        sc.nextLine();
        Project found = tpc.findProject(numWanted);
        if (found == null)
            System.out.println("No project with this number");
        else
        {
            System.out.println(found.toString());
            System.out.println(found.showTasks());
            System.out.print("Enter new task description :");
            String desc = sc.nextLine();
            System.out.print("Enter estimated hours :");
            int hours = sc.nextInt();
            sc.nextLine();
            Task t = found.addTask(desc, hours);
            if (t!=null)
                System.out.println("Added :" + t.toString());
            else
                System.out.println("Error. Task not added");
            }
      }
        
    /** 
     * A method to add an Employee
     * 
     */
    public static void addEmployee()
    {
        Employee newEmp;
        System.out.print("Enter employee name :");
        String name = sc.nextLine();
        System.out.println("(1) Part-time or (2) Full Time?");
        int empType = sc.nextInt();
        sc.nextLine();
        if (empType == 1)
        {
            System.out.print("Enter hourly rate :");
            double rate = sc.nextDouble();
            newEmp = new PartTimeEmployee(name, rate);
        }
        else
        {
            System.out.print("Enter monthly salary :");
            double salary = sc.nextDouble();
            newEmp = new FullTimeEmployee(name, salary);
        }
        if (tpc.addEmployee(newEmp))
            System.out.println("Employee added successfully");
        else
            System.out.println("Employee not added");
                
    }
    
    /** 
     * A method to show all employees
     */
    public static void showEmployees()
    {
        ArrayList<Employee> emplist = tpc.getEmployees();
        System.out.println("Do you want to sort by (1) Name or (2) Type");
        System.out.print("Enter choice :");
        int choice = sc.nextInt();
        if (choice == 1)
            Collections.sort(emplist);
        else
            Collections.sort(emplist, new TypeNameComparator());
        for (Employee e:emplist)
            System.out.println(e.toString());
    }
    /**
     *  A method to add a new project
     */
    public static void findProject()
    {
        System.out.println("Find by (1) name or (2) number");
        int findChoice = sc.nextInt();
        sc.nextLine();
        if (findChoice == 1)        
        {
            System.out.print("Enter name of project to find :");
            String projName = sc.nextLine();
            System.out.println(tpc.findProject(projName));
        }
        else
        {
            System.out.print("Enter project number of project to find :");
            int numWanted = sc.nextInt();
            sc.nextLine();
            Project found = tpc.findProject(numWanted);
            if (found != null)
                System.out.println("Found " + found.toString());
            else
                System.out.println("No project with this number");
        }
    }
    /**
     *  A method to add a new project
     */
    public static void addNewProject()
    {
        System.out.print("Enter name of project :");
        String projName = sc.nextLine();
        Project newProj = tpc.addProject(projName);
        if (newProj==null)
            System.out.println("TPC Capacity full.");
        else
        {
            System.out.println("New project created");
            System.out.print(newProj.getProjectNum() + "\t");
            System.out.println(newProj.getProjectName());
        }
    }
    
     /** 
     * A method to record hours worked for a Part Time Employee
     * 
     */
    public static void addTimesheetToEmployee()
    {
        System.out.print("Enter employee ID :");
        int ID = sc.nextInt();
        Employee emp = tpc.findEmployee(ID);
        if (emp == null)
            System.out.println("No employee with this ID");
        else if (!(emp instanceof PartTimeEmployee))
            System.out.println(emp.getName() +" is not a part timer!");
        else
        {
            PartTimeEmployee pt = (PartTimeEmployee) emp;
            System.out.println("Part timer " + emp.toString() +" found");
            System.out.println("Current status of " + pt.showTimesheets());
       
            System.out.print("Enter number of hours worked (0 to cancel):");
            int hours = sc.nextInt();
            if (hours > 0)
                pt.addTimesheet(hours);
        }
        
    }
    
     /** 
     * A method to process the payroll for all employees
     * 
     */
    public static void processPayroll()
    {
        double totalPay = tpc.payAllEmployees();
        System.out.println("All employees have been paid for a total payroll of " + totalPay);
    }
    
    /** 
     * Allow user to save the data and quit
     */
    public static void saveAndQuit()
    {
        System.out.print("Would you like to save before leaving?");
        char ans = sc.next().charAt(0);
        if (ans=='y' || ans=='Y')
        {
            save();
        }
        System.out.println("Goodbye!");
        
    }
    
    /** 
     *  Save to file
     * 
     */
    public static void save()
    {
	System.out.print("Enter filename :");
	String filename = sc.next();
	FileOutputStream fos=null;
	try 	
        {
            fos = new FileOutputStream(filename);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(tpc);
            oos.flush();
            oos.close();
	}
	catch (IOException ioe) {
            System.out.println(ioe.getMessage());
	}
	finally  {
            try{
		if (fos!=null) fos.close();
            }
            catch(IOException ioe)
            {
                System.out.println(ioe.getMessage());
	}
    }

        
    }
    
    public static void loadFile()
    {
        System.out.println("Do you want to load a previously saved file");
        char ans = sc.next().charAt(0);
        if (ans=='y' || ans=='Y')
        {
            System.out.print("Enter filename :");
            String filename = sc.next();
            FileInputStream fis=null;
            try {
                    fis = new FileInputStream(filename);
                    ObjectInputStream ois = new ObjectInputStream(fis);
                    tpc = (TPC) ois.readObject();

                    ois.close();
            }
            catch(Exception e) {
            System.out.println(e.getMessage());
            }
            finally {
                try {
                    if (fis!=null) 
                            fis.close();
                }
                catch(IOException ioe)
                {
                    System.out.println(ioe.getMessage());
                }
            }
        }
    }
        

    
}
