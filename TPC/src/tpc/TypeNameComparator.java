/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;
import java.util.Comparator;

/**
 *
 * @author ngsm
 */
public class TypeNameComparator implements Comparator<Employee> {

    @Override
    // compare two employees based on type, then name
    public int compare(Employee o1, Employee o2) {
        
        if (o1.getClass().equals(o2.getClass()))
        {
            return o1.getName().compareTo(o2.getName());
        }
        else
            if (o1 instanceof FullTimeEmployee)
                return -1;
            else 
                return 1;
    }
    
}
