/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;
import java.io.Serializable;
import java.util.ArrayList;

/* A class to represent <code>Project</code>
 * objects. A Project has a projectName and 
 * projectNum.
 * A project has an array of Task objects 
   and the number of tasks in the project 
    (maximum 100 allowed tasks)
 
 * @version 1.0 9/3/2015 
 * @author ngsm
 */
public class Project implements Serializable {
    
    private String projectName;
    private int projectNum;
    private ArrayList<Task> projectTasks;
    
    private static int nextProjectNo = 0;

    /** 
     * Constructor to create a new project
     * @param projectName
     * 
     */
    public Project(String projectName)
    {
        setProjectName(projectName);
        setProjectNum(nextProjectNo++);  
        projectTasks = new ArrayList<>();
            }
    
    /** 
     * setter for projectname
     * @param projectName 
     */
    public void setProjectName(String projectName)
    {
        this.projectName = projectName;
    }
    
    /** 
     * setter for projectnum
     * @param projectNum
     */
    private void setProjectNum(int projectNum)
    {
        this.projectNum = projectNum;
    }
    
    /** 
     * getter for projectName
     */
    public String getProjectName()
    {
        return projectName;
    }
    
    /** 
     * getter for projectNum
     */
    public int getProjectNum()
    {
        return projectNum;
    }
    
    public String toString()
    {
        return getProjectNum() + "-" + getProjectName();
    }
    
    /** a method to add a Task to the array
     * 
     */
    public Task addTask(String description, int estHours)
    {
        int numTasks = projectTasks.size();
        String taskNum = "P" + this.getProjectNum() + "-" + (numTasks+1);
        Task newTask = new Task(taskNum, description, estHours);
        projectTasks.add(newTask);
        return newTask;
    }
    /** 
     * a method to view all tasks. 
     */
    public String showTasks()
    {
        if (projectTasks.isEmpty())
            return "No Tasks Added Yet";
        String str = String.format("%s\t%-20s\t%s\t%s\t%s\n", "Task#", "Description","Est","Act","Status");
        for (Task t:projectTasks)
        {
            str += String.format("%s\t%-20s\t%d\t%d\t%s\n", t.getTaskNum(),t.getDescription(),t.getEstHours(),t.getActualHours(),t.getStatus());
        }
        return str;
    }

}

