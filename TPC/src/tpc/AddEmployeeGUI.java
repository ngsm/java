/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AddEmployeeGUI extends JFrame implements ActionListener 
{
	JButton getInfoBtn;
	JTextArea displayInfo;

	public AddEmployeeGUI()
	{
		setTitle("Employee Information");
		getInfoBtn = new JButton("Enter New Employee");
		displayInfo = new JTextArea(50, 20);
		
		JPanel btnPanel = new JPanel();
		btnPanel.add(getInfoBtn);
                getInfoBtn.addActionListener(this);
			
		// text area that displays all employees 
		JPanel dspPanel = new JPanel(new BorderLayout());
		dspPanel.add(new JLabel("Employees"), "North");
		displayInfo = new JTextArea(50, 20);
		dspPanel.add(new JScrollPane(displayInfo), "Center");

		getContentPane().add(btnPanel, "North");
		getContentPane().add(dspPanel, "Center");

		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public static void main(String[] args)
	{
		JFrame dialogEg = new AddEmployeeGUI();
		dialogEg.setBounds(10, 20, 400, 400);
		dialogEg.setVisible(true);
               
	}

    @Override
    public void actionPerformed(ActionEvent e) {
       EmployeeDialog inputDialog = new EmployeeDialog(this);
	inputDialog.pack();
	inputDialog.setLocationRelativeTo(this);
	inputDialog.setVisible(true);
			
	Employee p = inputDialog.getEmployee();
	if (p != null)
		displayInfo.append(p.toString() + "\n");
	else
		JOptionPane.showMessageDialog(this, "Employee not created!");
    }
}

