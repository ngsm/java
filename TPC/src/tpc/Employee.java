/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;

import java.util.Objects;
import java.io.Serializable;

/**
 *
 * @author ngsm
 */
public abstract class Employee implements Comparable<Employee>, Serializable {
    
    // instance variables
    private int empNum;
    private String name;
    
    private static int nextNo = 0;      // for autogenerate
    
    public Employee()
    {
       setEmpNum();
        this.name = "unknown";
    }
    
    public Employee(String name)
    {
        setEmpNum();
        this.name = name;
    }
    
    private void setEmpNum()
    {
        this.empNum = nextNo++;
    }
    public int getEmpNum()
    {
        return empNum;
    }
    
    public String getName()
    {
        return name;
        
    }
    
    public void setName(String name)
    {
        this.name = name;
    }

    @Override
    public int hashCode() {
       
        return this.getName().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
       
       
        if (this == obj)
            return true;
        
        if (!(obj instanceof Employee))
            return false;
        
        Employee rhs = (Employee) obj;  // downcast
        if ( this.getName().equals(rhs.getName()))
            return true;
         return false;
     }
    
    
    @Override
    public int compareTo(Employee o) {
       return this.getName().compareTo(o.getName());
    }
    abstract public String toString();
  
    abstract public double calculatePay();
    
}