/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;
import java.util.*;
import java.io.Serializable;
/**
 *
 * @author ngsm
 */
public class PartTimeEmployee extends Employee{
    
    private double hourlyRate;
    private ArrayList<Timesheet> ptEmpTimesheets;

    
    
    
    private class Timesheet implements Serializable
    {
	private boolean isPaid;	// record whether emp has been paid for this
	private int hours;		// number of hours worked

	// complete the inner class with constructor, getters, setters, toString.
             
	public Timesheet()
        {
            isPaid = false;
            hours= 0;
        }
        
        public Timesheet(int hours)
        {
            this.hours = hours;
        }

        public boolean getIsPaid() {
            return isPaid;
        }

        public void setIsPaid(boolean isPaid) {
            this.isPaid = isPaid;
        }

        public int getHours() {
            return hours;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }
        
        public String toString()
        {
            return hours + " hours " + (isPaid?"paid": "not paid yet");
        }
    }

  
    public PartTimeEmployee()
    {
        this("unknown", 0.0);
        ptEmpTimesheets = new ArrayList<>();
    }
    public PartTimeEmployee(String name, double hourlyRate)
    {
        super(name);
        setHourlyRate(hourlyRate);
        ptEmpTimesheets = new ArrayList<>();
   
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        if (hourlyRate >= 0)
            this.hourlyRate = hourlyRate;
    }
    
    public void addTimesheet(int hours)
    {
        Timesheet t = new Timesheet(hours);
        ptEmpTimesheets.add(t);
    }
    @Override
     public String toString()
    {
        return getName() + " is Part Timer: Emp Num: " + getEmpNum()   + " and hourly rate of " + hourlyRate;
    }
    
     public String showTimesheets()
     {
         String allTimesheets = "All timesheets for " + getName() + ":\n";
         for (Timesheet t: ptEmpTimesheets)
        {
            allTimesheets += t.toString() + "\n";
        }
         return allTimesheets;
     }
     @Override
    public double calculatePay() {
        // get unpaid hours and set timesheets to paid
    
        
        int totalUnpaidHours = 0;
        for (Timesheet t: ptEmpTimesheets)
        {
            if (!t.getIsPaid())
            {    totalUnpaidHours += t.getHours();
                 t.setIsPaid(true);
            }
        }
        // calculate total pay due
       return totalUnpaidHours * this.getHourlyRate();
    }
    
    
}
