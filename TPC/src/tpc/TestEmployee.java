/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tpc;

/**
 *
 * @author ngsm
 */
import java.util.*;
import java.io.*;

public class TestEmployee {


        static Scanner sc = new Scanner(System.in);
    
	public static void main(String[] args) {
	
		FullTimeEmployee e1 = new FullTimeEmployee("Mary",100);
                FullTimeEmployee e2 = new FullTimeEmployee("Ali", 200);
                PartTimeEmployee e3 = new PartTimeEmployee("Ahmad", 6.5);
                PartTimeEmployee e4 = new PartTimeEmployee("Joe", 4.5);
                 
                ArrayList<Employee> emplist = new ArrayList<>();
                emplist.add(e1);
                emplist.add(e2);
                emplist.add(e3);
                emplist.add(e4);

  
                emplist.add(new FullTimeEmployee("Jack", 500));
                System.out.println("Before sorting:");
                for (Employee e: emplist)
                {
                    System.out.println(e.toString());
                }
                Collections.sort(emplist, new TypeNameComparator());
                
                System.out.println("After sorting:");
                for (Employee e: emplist)
                {
                    System.out.println(e.toString());
                }
                
                writeEmployees(emplist);
                    
                    
        }
        
    public static boolean writeEmployees(ArrayList<Employee> emplist)
    {
        System.out.println("Enter name of file to save to");
        String fileName = sc.nextLine();
        File file = new File(fileName);
        FileWriter fw = null; 
        try
        {
            if (file.exists())
            {
                System.out.println("File '" + fileName + "' already exists.");
                System.out.print("Choose to (o)verwrite or (a)ppend: ");
                char response = sc.nextLine().charAt(0);
                if (response == 'o' || response == 'O')
                        fw = new FileWriter(fileName); // throws IOException
                else
                        fw = new FileWriter(fileName, true); // append
            }
            else
                    fw = new FileWriter(fileName);   // file doesn’t exist

            BufferedWriter bw = new BufferedWriter(fw);

            for (Employee e:emplist)
            {
                if (e instanceof PartTimeEmployee)
                {
                    PartTimeEmployee pt = (PartTimeEmployee) e;
                    bw.write("pt" + ",");
                    bw.write(pt.getName() + "," + pt.getHourlyRate());
                }
                else
                {
                    FullTimeEmployee ft = (FullTimeEmployee) e;
                    bw.write("ft" + ",");
                    bw.write(ft.getName() + "," + ft.getMonthlySalary());
                }
                bw.newLine(); // throws IOException
        
            }
           
            bw.flush(); // throws IOException
            bw.close(); // throws IOException
        }
        catch (IOException e)
        {
            System.out.println(e.getMessage());
            return false;
        }
        finally
        {
            try// if the file was opened, close it
            {
                if (fw != null)
                fw.close();
            }
            catch (IOException ioe)
            {
		System.out.println(ioe.getMessage());
		return false;
            }   

        }
        return true;	// no exceptions thrown
}   	// end of method


  
}
