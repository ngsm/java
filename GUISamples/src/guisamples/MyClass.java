/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guisamples;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
/**
 *
 * @author ngsm
 */
public class MyClass extends JFrame implements MouseListener {

    private JLabel lbl;
    private JButton okButton;
    private int numTimes;
    
    public MyClass()
    {
      
        super("Testing Mouse Click");
        setSize(100,100);
          numTimes = 0;
        setLayout(new FlowLayout());
        okButton = new JButton("OK");
        add(okButton);
        setVisible(true);
        okButton.addMouseListener(this);
        lbl = new JLabel("Button clicked 0 times");
        add(lbl);
    }
    
    public static void main(String[] args)
    {
        MyClass frame = new MyClass();
        frame.setVisible(true);
    }

  

    @Override
    public void mouseClicked(MouseEvent e) {
         numTimes++;
        lbl.setText("Button clicked " + numTimes + " times");
        okButton.setText("Don't click me again");
    
         }

    @Override
    public void mousePressed(MouseEvent e) {
     }

    @Override
    public void mouseReleased(MouseEvent e) {
     }

    @Override
    public void mouseEntered(MouseEvent e) {
           }

    @Override
    public void mouseExited(MouseEvent e) {
      }

   
}
