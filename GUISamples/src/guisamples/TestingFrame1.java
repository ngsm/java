/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package guisamples;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author ngsm
 */
public class TestingFrame1 {
    
    public static void main(String[] args)
    {
        JFrame jf = new JFrame();
        jf.setSize(200, 300);
        
        jf.setLayout(new FlowLayout());
        JLabel nameLbl = new JLabel("Name");
        JTextField nameTF = new JTextField(30);
        JButton okButton = new JButton("OK");
        
        jf.add(nameLbl);
        jf.add(nameTF);
        jf.add(okButton);
        
        jf.setVisible(true);
    
    }
}
