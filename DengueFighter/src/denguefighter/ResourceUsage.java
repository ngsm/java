/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denguefighter;

/**
 *
 * @author ngsm
 */
public class ResourceUsage {

    private Resource theResource;
    private Site theSite;
    private int weekNo;
    private int numUnitsPurchased;

    public ResourceUsage(Resource r, Site s, int weekNo, int numUnitsPurchased) {
        this.theResource = r;
        this.theSite = s;
        this.weekNo = weekNo;
        this.numUnitsPurchased = numUnitsPurchased;
    }

     public int getWeekNo() {
        return weekNo;
    }

    public void setWeekNo(int weekNo) {
        this.weekNo = weekNo;
    }

    public int getNumUnitsPurchased() {
        return numUnitsPurchased;
    }

    public void setNumUnitsPurchased(int numUnitsPurchased) {
        this.numUnitsPurchased = numUnitsPurchased;
    }

    

    public Resource getResource() {
        return theResource;
    }

    public void setResource(Resource theResource) {
        this.theResource = theResource;
    }

    public Site getSite() {
        return theSite;
    }

    public void setSite(Site theSite) {
        this.theSite = theSite;
    }

    
        @Override
    public int hashCode() {
       
        return this.getResource().hashCode() + this.getSite().hashCode() + this.getWeekNo();
    }

    @Override
    /** 
     * Two Resource Usage objects are equal if they
     * refer to the same resource and the same week for the same site
     * @param obj
     * @return 
     */
    public boolean equals(Object obj) {
       
       
        if (this == obj)
            return true;
        
        if (!(obj instanceof ResourceUsage))
            return false;
        
        ResourceUsage rhs = (ResourceUsage) obj;  // downcast
        // ResourceUsage objects are equal if they refer to the same object, site and 
        // weekNo
        if ( this.getResource().equals(rhs.getResource()) &&
                this.getSite().equals(rhs.getSite()) && this.getWeekNo() == rhs.getWeekNo())

            return true;
         return false;
     }
  
    public String applyResource()
    {
        // apply the resource the number of times it is purchased
        String result = "";
        result += "Applying " + theResource.toString() + "\n";
        for (int i = 0; i < numUnitsPurchased; i++)
        {
            theResource.applyTo(theSite);
        }
        return result;
    }

    @Override
    public String toString() {
        return "Week No : " + weekNo +  " Resource : " + theResource.getResourceID() +  " Num Units : " + numUnitsPurchased;
           
    }
    
    
}
