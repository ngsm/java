package denguefighter;
import java.util.*;
/**
 * Site represents a site that you are responsible for.
 * @author ngsm
 */
public class Site {
     
    // Current infection rate in Selangor is 0.15 - 0.27
    // http://caring.ridpest.com/the-geographical-distribution-of-dengue-fever/
    // we'll just use 0.15
    private static final double INFECTRATE = 0.15;
    
    // Random number generator to set up a site
    private static Random randomGenerator = new Random();
   
    private String siteID;
    private int population;
    private double revenuePerWorker;
    private Grade cleanliness;
    private boolean hasAedes;
    private int numInfected;
    private static int nextNo = 100; 
    private ArrayList<ResourceUsage> ru;
    
    /**
     * Constructor to initialize size and infection rate
     */

    public Site()
    {
        siteID = "S" + nextNo++;
       
       
        // population is between 500 and 1000 
        population = 501 + randomGenerator.nextInt(2000);
        
        // average revenue earned by each uninfected member of the population
        // between $0.1 and $1 per week
        revenuePerWorker = randomGenerator.nextDouble()+0.1;
        
        // cleanliness is randomly determined
        cleanliness = Grade.getRandomGrade();
        
        // randomly determine if the site has aedes
        calculateAedesByCleanliness();
       
        // first assume nobody is infected
        numInfected = 0;
        
        // update number infected based on cleanliness and presence of Aedes
        updateInfections();
        
        ru = new ArrayList<>();
    }

    public String getSiteID() {
        return siteID;
    }
    /**
     * @return the population
     */
    public int getPopulation() {
        return population;
    }

    
    /**
     * @param population the population to set
     */
    public void setPopulation(int population) {
        this.population = population;
    }

    /**
     * @return the numInfected
     */
    public int getNumInfected() {
        return numInfected;
    }

    /**
     * @param numInfected the numInfected to set
     */
    public void setNumInfected(int numInfected) {
        this.numInfected = numInfected;
    }

    /**
     * @return the cleanliness
     */
    public Grade getCleanliness() {
        return cleanliness;
    }

    /**
     * @param cleanliness the cleanliness to set
     */
    public void setCleanliness(Grade cleanliness) {
            this.cleanliness = cleanliness;
    }



    /**
     * @return the hasAedes
     */
    public boolean hasAedes() {
        return hasAedes;
    }

    /**
     * @param hasAedes the hasAedes to set
     */
    public void setHasAedes(boolean hasAedes) {
       
            this.hasAedes = hasAedes;
        }
        
    private void calculateAedesByCleanliness()
    {
        // assign weights to cleanliness and probability
        final double PROBABILITYWEIGHT = 0.50;  // normal probability affects 50%
        final double CLEANLINESSWEIGHT = 0.50;  // cleanliness affects 50%
        
        double cleanScore = cleanliness.getMultiplier()/5;
        double probability = randomGenerator.nextDouble();
        
        // determine probability based on weights assigned

        
        double totalProbability = cleanScore*CLEANLINESSWEIGHT + probability*PROBABILITYWEIGHT;
       setHasAedes(totalProbability < 0.5);
         
    }
    
    /**
     * Get the weekly revenue of uninfected workers
     * @return the revenue of workers who are not infected
     */
    public double getRevenue()
    {
        return revenuePerWorker * (population - numInfected);
    }
    
       public int hashCode() {
       
        return this.getSiteID().hashCode();
    }

    @Override
    /** 
     * Two Site objects are equal if they
     * have the same siteID
     * @param obj
     * @return 
     */
    public boolean equals(Object obj) {
       
       
        if (this == obj)
            return true;
        
        if (!(obj instanceof Site))
            return false;
        
        Site rhs = (Site) obj;  // downcast
        return this.getSiteID().equals(rhs.getSiteID());

      }
    @Override
    public String toString()
    {
        return siteID + " : Cleanliness " + cleanliness  +
                "\nPopulation : " + population + 
                "\nRevenue Per Worker : " + String.format("$%.2f",revenuePerWorker) +  
                "\nNum Infections : " + numInfected + 
                "\nhas Aedes : " + hasAedes +"\n";
                
    }
    
    
    /**
     * probability of infection is calculated
     * based on cleanliness of site: the cleaner, the lower
     * probability
     */
    public final void updateInfections()
    {
        // probability of infection depends inversely on cleanliness
        // and the proportion of people already infected
       if (hasAedes)    // if aedes still present, recalculate infections
       {
            double probability = (1.0/cleanliness.getMultiplier()) * (((double) numInfected)/population + 1);
            int newInfections = (int) (0.15 * probability * (population - numInfected) );
            // update infection rate
            numInfected += newInfections;  
       }
       else // no aedes, update hasAedes based on cleanliness
       {
            calculateAedesByCleanliness();
       }
       
      
       
    }
    
  
    /**
     * @return the revenuePerWorker
     */
    public double getRevenuePerWorker() {
        return revenuePerWorker;
    }

    /**
     * @param revenuePerWorker the revenuePerWorker to set
     */
    public void setRevenuePerWorker(double revenuePerWorker) {
       if (revenuePerWorker > 0)
        this.revenuePerWorker = revenuePerWorker;
    }
    
    public boolean addResourceUsage(Resource r, int numUnits, int weekNo)
    {
        ResourceUsage resU = new ResourceUsage(r, this, weekNo, numUnits);
        // check if resourceUsage already exists
        // for this site, resource and weekNo
        // example of using Contains
        if (ru.contains(resU))
                return false;
          
        // if not, create new ResourceUsage
         ru.add(resU);
       
        return true;
    }
    
    /**
     * This method applies the weekly resources assigned 
     * and returns a string explaining what was applied.
     * @param weekNo
     * @return a String explaining what was applied
     */
    public String applyWeeklyResources(int weekNo)
    {
        String result = "For site: " + getSiteID() + "\n";
        boolean found = false;
        for (ResourceUsage resU: ru)
        {
            if (resU.getWeekNo()==weekNo)
            {
                found = true;
                result += "\t" + resU.applyResource();
        
            }
        }
        if (!found)
            result += " No resources assigned this week\n";
        return result;
    }
    
    /** A method to show the resources currently assigned
     * 
     */
    public String currentResourceUsage()
    {
        boolean foundRU = false;
        String results = "Resource Usage History\n";
        for (ResourceUsage resU:ru)
        {
                foundRU = true;
                results += resU.toString() + "\n";
        }
        if (foundRU)
            return results;
        else
            return "No resources assigned yet";
    }
}
