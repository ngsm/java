/*
 * DengueGame 
 */
package denguefighter;


/**
 * The resource class is an abstract superclass that defines the type of resources
 * that can be purchased for to combat dengue. 
 * @author ngsm
 */
abstract public class Resource {
    
    
    private String resourceID;
    private String resourceName;
    private double cost;
    
    public Resource(int resourceNo, String name, double cost)
    {
        this.resourceID = "R"+resourceNo;
        this.resourceName = name;
        this.cost= cost;
      }

    /**
     * @return the resourceID
     */
    public String getResourceID() {
        return resourceID;
    }

    /**
     * @return the name
     */
    public String getName() {
        return resourceName;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.resourceName = name;
    }

    /**
     * @return the costPerUnit
     */
    public double getCost() {
        return cost;
    }

    /**
     * @param cost the costPerUnit to set
     */
    public void setCost(double cost) {
        this.cost = cost;
    }

    /**
     * @return the unitOfMeasurement
     */
     
       public int hashCode() {
       
        return this.getResourceID().hashCode();
    }

    @Override
    /** 
     * Two Resource objects are equal if they
     * refer have the same resourceID 
     * @param obj
     * @return 
     */
    public boolean equals(Object obj) {
       
       
        if (this == obj)
            return true;
        
        if (!(obj instanceof Resource))
            return false;
        
        Resource rhs = (Resource) obj;  // downcast
         return this.getResourceID().equals(rhs.getResourceID());
     }
    abstract public void applyTo(Site theSite);
    
    @Override
    public String toString()
    {
        return  String.format("%4s\t%-20s\t$%5.2f",this.resourceID, this.resourceName, this.cost);
    }
}