/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denguefighter;
import java.util.*;
/**
 *
 * @author ngsm
 */
public class DengueFighter {

    private String playerName;
    private double currentBudget;
    private int currentWeek;
    private ArrayList<Site> sites;
    private ArrayList<Resource> resources;

    public DengueFighter(String player)
    {
        this.playerName = player;
        currentBudget = 0;
        currentWeek = 0;
        sites = new ArrayList<>();
        resources = new ArrayList<>();
    }

    public double getCurrentBudget() {
        return currentBudget;
    }

    public void setCurrentBudget(double currentBudget) {
        if (currentBudget >= 0)
            this.currentBudget = currentBudget;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public int getCurrentWeek() {
        return currentWeek;
    }

    public void setCurrentWeek() {
        this.currentWeek++;
    }
    
    
    public DengueFighter(ArrayList<Site> sites, ArrayList<Resource> resources) {
        this.sites = sites;
        this.resources = resources;
    }
    
    public void addSite(Site s)
    {
        sites.add(s);
        
    }
    
    public void addResource(Resource r)
    {
        resources.add(r);
    }
    
    
    /**
     *
     * @param resourceID
     * @return 
     */
    public Resource getResource(String resourceID)
    {
        for (Resource r:resources)
        {
            if (r.getResourceID().equals(resourceID))
                return r;
        }
        return null;
    }
    
    public Site getSite(String siteID)
    {
        for (Site s:sites)
        {
            if (s.getSiteID().equals(siteID))
                return s;
        }
        return null;
    }
    
    /** A method to update site infections for the week
     * 
     * @return 
     */
    public void updateSitesInfections()
    {
        for (Site s:sites)
        {
            s.updateInfections();
        }
        
    }
    public String showAllSites()
    {
        String str = "SiteID\tCleanliness\tPopn\tInfections\tRevenue\n";
        str+="------\t-----------\t----\t----------\t-------\n";
        for (Site s:sites)
        {
            str += s.getSiteID() + "\t" + s.getCleanliness() + "\t\t" ;
            str += s.getPopulation() + "\t\t" + s.getNumInfected() +"\t$";
            str += String.format("%.2f", s.getRevenue())+ "\n";
        }
        return str;
    }
    
    public String showAllResources()
    {
        String str = String.format("%4s\t%-20s\t%5s\t%s\t%s\n", "ResID","Resource Name","Cost","#Weeks","Description");
        str+=String.format("%4s\t%-20s\t%5s\t%s\t%s\n", "-----","-------------","----","------","-----------");
        
        for (Resource r:resources)
        {
               str += r.toString() +"\n";
         }
        return str;
    }
    
    public double totalRevenue()
    {
        double total = 0;
        total = sites.stream().map((s) -> s.getRevenue()).reduce(total, Double::sum);
        return total;
    }
    
    
    public boolean assignResource(Site s, Resource r, int numUnits)
    {
       // assign resource to site
        
        boolean assigned = s.addResourceUsage(r, numUnits, currentWeek);
                        // false  if already assigned        
        if (!assigned)
           return false;
        
         //     true if success, reduce budget by amount spent
        double spent = r.getCost() * numUnits;
        setCurrentBudget(this.currentBudget - spent);
        return true;
    }
    
    /**
     * Apply all resources for the current week
     */
    public String applyResources()
    {
        String result = "";
        for (Site s:sites)
        {
            result += s.applyWeeklyResources(currentWeek);
        }
        return result;
    }
}
