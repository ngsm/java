/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denguefighter;
import java.util.*;
/**
 *
 * @author ngsm
 */
public class DTConsole {
    
    static Scanner sc = new Scanner(System.in);
    static DengueFighter dt;
    static int week = 0;
    
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Welcome! What is the player name?");
        String player = sc.nextLine();
        
        dt = new DengueFighter(player);
        setup();
        char cont;
        do
        {
            weeklyRound();
            System.out.printf("Your current budget is $%.2f\n", dt.getCurrentBudget());
        
            System.out.println("Do you want to continue? Y/N");
            cont = sc.next().charAt(0);
            
         } while(cont == 'y' || cont == 'Y');
        
    }
    
    public static void weeklyRound()
    {
       
        System.out.println(dt.getPlayerName() + ", press enter to start");
        sc.nextLine();        
        dt.setCurrentWeek();
        dt.updateSitesInfections();
        System.out.println("Week : " + dt.getCurrentWeek());
        System.out.printf("This week's revenue is $%.2f\n", dt.totalRevenue());
        dt.setCurrentBudget(dt.getCurrentBudget()+dt.totalRevenue());
         menu();
        
        }
    
    public static void menu()
    {
       
        int choice;
        do
        { 
            System.out.println("Current sites status");
            System.out.println(dt.showAllSites());
            System.out.printf("Your current budget is $%.2f\n" , dt.getCurrentBudget());
       
            
            System.out.println("Would you like to:");
            System.out.println("1. Manage site");
            System.out.println("0. End Week");
            System.out.print("Enter choice :");
            choice = sc.nextInt();
            switch(choice)
            {
                case 1: assignResource(); break;
                case 0: applyResources(); break;
                default: System.out.println("Error");

            }
        } while (choice!=0);
    }
    
    
    
    public static void assignResource()
    {
        System.out.print("Enter site ID :");
        String id = sc.next();
        Site s = dt.getSite(id);
        if (s==null)
            System.out.println("Site " + id + " not found");
        else
        {
            System.out.println(s.toString());
            System.out.println(s.currentResourceUsage());
            System.out.println("\n Available Resources to Purchase");
            System.out.println(dt.showAllResources());
            System.out.print("Enter resource ID :");
            String rid = sc.next();
            Resource r = dt.getResource(rid);
            if (r==null)
                System.out.println("Resource not found");
            else 
                
            {
                System.out.print("How many units to purchase? ");
                int numUnits = sc.nextInt();
                double totalCost= numUnits * r.getCost();
               // should put this in DengueFighter
                if (totalCost > dt.getCurrentBudget())
                {
                    System.out.println("Not enough money!");
                }
                else
                {
                    // assign resource 
                    if (dt.assignResource(s, r, numUnits))
                    {
                        System.out.println("Assigned " + r.getName() + " to " + s.getSiteID() + " for " + totalCost);
                        System.out.printf("You have %5.2f left\n", dt.getCurrentBudget());
                    }
                    else
                        System.out.println("Resource already assigned for the week");
                }    
            }
    
        }
    }
    
    public static void setup()
    {
        for (int i = 0; i < 5; i++)
        {
            dt.addSite(new Site());
        }
        
        dt.addResource(new Pesticide("Larvicide", 100, Grade.MEDIUM, Grade.LOW ));
        dt.addResource(new Pesticide("Fogging", 200, Grade.LOW, Grade.MEDIUM ));
        dt.addResource(new Pesticide("Residual Spray", 300, Grade.HIGH, Grade.HIGH ));
        dt.addResource(new MedicalService("General Ward", 100,  10));
        dt.addResource(new MedicalService("Isolation Ward", 300,  20));
        dt.addResource(new CleanupTeam("Basic Cleaning", 100,  5, Grade.LOW));
        dt.addResource(new CleanupTeam("General Cleaning", 200,  10, Grade.MEDIUM));
        dt.addResource(new CleanupTeam("Detailed Cleaning", 200,  10, Grade.HIGH));
       
        
    }
    
    public static void applyResources()
    {
        System.out.println("Ending the week " + dt.getCurrentWeek());
        System.out.println("Applying resources now: ");
        System.out.println(dt.applyResources());
        System.out.println("All resources applied");
    }
}
