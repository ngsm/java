/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denguefighter;

/**
 *
 * @author ngsm
 */
public class MedicalService extends Resource{
    
    private int patientCapacity;
    private static int nextNo = 200;
    
    public MedicalService( String name, double cost, int patientCapacity) {
        super(nextNo++, name, cost);
            // medical services only used for one week
        this.patientCapacity = patientCapacity;
    }

    public int getPatientCapacity() {
        return patientCapacity;
    }

    public void setPatientCapacity(int patientCapacity) {
        this.patientCapacity = patientCapacity;
    }

    
    @Override
    public void applyTo(Site theSite) {
        int numInfected = theSite.getNumInfected();
        if (patientCapacity >= numInfected)
            theSite.setNumInfected(0);
        else
            theSite.setNumInfected(numInfected-patientCapacity);
    }
    @Override
     public String toString() {
        return super.toString() + "\tpatient capacity=" + patientCapacity;
    }
 
}
