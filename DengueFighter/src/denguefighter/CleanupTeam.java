/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denguefighter;

/**
 *
 * @author ngsm
 */
public class CleanupTeam extends Resource {
    
    private int numCleaners;
    private Grade capability;
    private static int nextNo = 300;
    

    public CleanupTeam(String name, double cost, int numCleaners, Grade capability) {
        super(nextNo++, name, cost);
        this.numCleaners = numCleaners;
        this.capability = capability;
    }

    public int getNumCleaners() {
        return numCleaners;
    }

    public void setNumCleaners(int numCleaners) {
        this.numCleaners = numCleaners;
    }

    public Grade getCapability() {
        return capability;
    }

    public void setCapability(Grade capability) {
        this.capability = capability;
    }

    @Override
    public void applyTo(Site theSite) {
       // cleanup team increases cleanliness of site based on capability
        Grade currentGrade = theSite.getCleanliness();
        
        int numberOfUpgrade = (capability.getMultiplier())/2;
        for (int i = 0; i < numberOfUpgrade; i++ )
        {
            currentGrade = Grade.nextGrade(currentGrade);
        }
        theSite.setCleanliness(currentGrade);
    }
    
    @Override
     public String toString() {
        return super.toString() + "\tworkers=" + numCleaners + ", capability=" + capability;
    }
 
    
    
}
