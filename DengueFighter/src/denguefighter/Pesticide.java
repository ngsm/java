/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package denguefighter;
import java.util.*;
/**
 *
 * @author ngsm
 */
public class Pesticide extends Resource{
    
    private static int nextNo = 100;
    
    private Grade toxicity;
    private Grade effectiveness;

    public Pesticide(String name, double cost, Grade toxicity, Grade effectiveness) {
        super(nextNo++, name, cost);
        this.toxicity = toxicity;
        this.effectiveness = effectiveness;
    }

    public Grade getToxicity() {
        return toxicity;
    }

    public void setToxicity(Grade toxicity) {
        this.toxicity = toxicity;
    }

    
  


    /**
     * @return the effectiveness
     */
    public Grade getEffectiveness() {
        return effectiveness;
    }

    /**
     * @param effectiveness the effectiveness to set
     */
    public void setEffectiveness(Grade effectiveness) {
        this.effectiveness = effectiveness;
    }

    @Override
    public String toString() {
        return super.toString() + "\ttoxicity=" + toxicity + ", effectiveness=" + effectiveness;
    }
 
    @Override
    public void applyTo(Site theSite)
    {
        Random r = new Random();
        // when applying pesticide
        // Higher effectiveness reduces Aedes probability
        double effectScore = effectiveness.getMultiplier()/5;
        double probability = r.nextDouble();
        
        // determine probability based on weights assigned

        double totalProbability = effectScore*0.5 + probability*0.5;
        theSite.setHasAedes(totalProbability < 0.5);
        
        // revenue per worker declines by 0.1 per toxicity multiplier
        // formula is 1.1 - (multiplier)/10
        
        double reduction = 1.1-(0.1*toxicity.getMultiplier());
        theSite.setRevenuePerWorker(theSite.getRevenuePerWorker()*reduction);
        
    }
          
}
