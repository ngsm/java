/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IntroducingGUI;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Vector;

public class GreetingGui extends JFrame {

	// Text field and a button
	JTextField lastNameTF;
	JTextField firstNameTF;
	JButton OKbtn; 
	
	public GreetingGui()
	{
            
            JList jlist = new JList(new Vector());
 		// labels for the text 
 		// field in 1 column
 		// red panel shown in diagram
		JPanel p1 = new JPanel(new GridLayout(0,1,0,8));
		p1.add(new Label("First Name"));
		p1.add(new Label("Last Name"));
		
		// text fields in another column
 		// red panel shown in diagram
		JPanel p2 = new JPanel(new GridLayout(0,1,0,8));
		firstNameTF = new JTextField(10);
		lastNameTF = new JTextField(10);
		p2.add(firstNameTF);
		p2.add(lastNameTF);
				
		// put both columns in one panel with FlowLayout
		// blue one shown in diagram
		JPanel p3 = new JPanel();
		p3.add(p1);
		p3.add(p2);
		
		// buttons in another panel – blue one in diagram
		JPanel p4 = new JPanel();
		OKbtn = new JButton("OK");
		p4.add(OKbtn);
                OKbtn.addMouseListener(new MouseAdapter()
                {  // anonymous class

                    public void mouseClicked(MouseEvent e) {
                    String fname = firstNameTF.getText();
                    String lname = lastNameTF.getText();
                    String fullname = fname + " " + lname;
                    JOptionPane.showMessageDialog(rootPane, "Hello " + fullname);
                    }
                }
                );
               
		
		// put both panels in 1 column, green outline
		JPanel p5 = new JPanel(new GridLayout(2,0));
		p5.add(p3);
		p5.add(p4);
		
		// set up the JFrame
		setTitle("Greeting");
		setLayout(new BorderLayout());
		getContentPane().add(p5, BorderLayout.NORTH);
		setSize(300,150);
		setLocation(500, 200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
       
       
	// driver program to test the JFrame
	
        public static void main(String[]args)
	{
		JFrame ff = new GreetingGui();
		ff.setVisible(true);
	}

    
}
