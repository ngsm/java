/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IntroducingGUI;
import java.awt.Component;
import javax.swing.*;

public class FirstSwing
{
   public static void main(String[] args)
   {
	JFrame frame = new JFrame("My Frame");
        frame.setSize(200,100);
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	JLabel label = new JLabel("Welcome to Swing");
        frame.getContentPane().add(label);
		
	frame.setVisible(true);
   }
}
