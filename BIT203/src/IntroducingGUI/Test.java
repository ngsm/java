/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IntroducingGUI;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.*;
import java.awt.event.*;
/**
 *
 * @author ngsm
 */
public class Test extends JFrame implements ActionListener{
    JLabel title = new JLabel("Mr.");
        JTextField name = new JTextField(10);
        JRadioButton maleRB = new JRadioButton("Male");
        JRadioButton femaleRB = new JRadioButton("Female");
        
    public Test()
    {
        
        ButtonGroup bg= new ButtonGroup();
        bg.add(maleRB);
        bg.add(femaleRB);
        maleRB.addActionListener(this);
        femaleRB.addActionListener(this);
        JPanel p1 = new JPanel();
        p1.add(title);
        p1.add(name);
        JPanel p2 = new JPanel();
        p2.add(maleRB);
        p2.add(femaleRB);
        JPanel p3 = new JPanel(new GridLayout(0,1,0,8));
        p3.add(p1);
        p3.add(p2);
        setTitle("Greeter");
        setSize(200,100);
        add(p3, BorderLayout.NORTH);
        setVisible(true);
        
        
        
    }
    
    public void actionPerformed(ActionEvent e)
    {
       if (maleRB.isEnabled())
           title.setText("Mr.");
       else
           title.setText("Ms.");
    }
    public static void main(String[] args)
    {
        Test t = new Test();
        
    }
    
}
