/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package IntroducingGUI;
import java.awt.event.*;

import javax.swing.JOptionPane;

public class GreetingHandler implements MouseListener{

	private GreetingGui thegui;		// to access view components	
	
	// constructor that initializes the view
	public GreetingHandler(GreetingGui gg)
	{
		thegui =gg;
		
	}

	public void mouseClicked(MouseEvent e) {
		String fname = thegui.firstNameTF.getText();
		String lname = thegui.lastNameTF.getText();
		String fullname = fname + " " + lname;
		JOptionPane.showMessageDialog(thegui, "Hello " + fullname);
	}

	public void mouseEntered(MouseEvent e) {}

	public void mouseExited(MouseEvent e) {}

	public void mousePressed(MouseEvent e) {}

	public void mouseReleased(MouseEvent e) {}
}
