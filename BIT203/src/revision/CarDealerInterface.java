/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package revision;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;
public class CarDealerInterface extends JFrame {
	
        private CarDealer cd;
	private CarDealerModel cdModel;
	private JTable cdTable;
	private CarInterface ci;
	private JButton addBtn;
        private DefaultTableModel tableModel;
	
		/*
	Hint: 
		constructor that takes a CarDealer object as parameter
		instantiation of various objects – model as well as GUI
		dealing with event generated – adding of Car objects
		layout of GUI components
		etc...
		Refer to diagram on previous page that shows an instance of
		      CarDealerInterface class in a driver program
	 */	
        public CarDealerInterface(CarDealer cd)
        {
            // Question 6 (b) complete the constructor
            this.cd = ??
            addBtn = new JButton("Add Car");
            cdModel = new ??
            cdTable = new ??;
            
            ci = new CarInterface(this, true);
            add(addBtn, BorderLayout.NORTH);
            add(new JScrollPane(cdTable), BorderLayout.CENTER);
            addBtn.addActionListener(new ActionListener()
            {
                public void actionPerformed(ActionEvent e)
                {
                    ci.setVisible(true); 
                    if (ci.getStatus()==false)
                        JOptionPane.showMessageDialog(null, "Car not added");
                }
            });
        }
        
        public CarDealerModel getCarDealerModel()
        {
            return cdModel;
        }
        
        public static void main(String[] args)
        {
            CarDealerInterface cdi = new CarDealerInterface(new CarDealer());
            cdi.setSize(400,200);
            cdi.setVisible(true);
        }
}
