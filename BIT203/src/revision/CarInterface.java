/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package revision;
import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
class CarInterface extends JDialog implements ActionListener
{
	private CarDealerModel cd;
        private boolean status;
        // GUI components set up go here
        JLabel regNoLabel, makeLabel, yearMadeLabel, priceLabel;
        JTextField regNoTF, makeTF, yearMadeTF, priceTF;
        JButton okBtn, cancelBtn;
	// constructor
	
        public CarInterface(JFrame parent, boolean modal)
	{
		super(parent, true);	//creates a modal dialog
		cd= ((CarDealerInterface) parent).getCarDealerModel();		// initialize car
	
		regNoLabel = new JLabel("Reg. No");
		makeLabel = new JLabel("Make ");
		yearMadeLabel = new JLabel("Year Made");
                priceLabel = new JLabel("Price");
                regNoTF = new JTextField(10);
		makeTF = new JTextField(10);
                yearMadeTF = new JTextField(10);
                priceTF = new JTextField(10);
		okBtn = new JButton("OK");
                cancelBtn = new JButton("Cancel");
                // register the buttons for actionlistener
                okBtn.addActionListener(this);
                cancelBtn.addActionListener(this);
                
		JPanel p1 = new JPanel(new GridLayout(0,1,0,8));
		p1.add(regNoLabel);
                p1.add(makeLabel);
                p1.add(yearMadeLabel);
		p1.add(priceLabel);
		
		JPanel p2 = new JPanel(new GridLayout(0,1,0,8));
		p2.add(regNoTF);
		p2.add(makeTF);
                p2.add(yearMadeTF);
                p2.add(priceTF);
		
		JPanel p3 = new JPanel();
		p3.add(p1);
		p3.add(p2);
		
		JPanel p4 = new JPanel();
		p4.add(okBtn);
		p4.add(cancelBtn);
		
		JPanel p5 = new JPanel(new GridLayout(0,1));
		p5.add(p3);
		p5.add(p4);
		
		Container cp = getContentPane();
		cp.add(p5, BorderLayout.NORTH);
		
		setTitle("New Car");
		setSize(300,200);
		setLocation(80,200);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		
	}
	

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == okBtn)
        {
          try {  
            // complete this part: get the text that was input
            String regNo = ???
            String make = ???
            int yearMade = ???
            double price = ???
            
            // then you can create a new car
            Car car = new Car(regNo, make, yearMade, price);
            
            // add the car to the carDealerModel
            status = cd.add(car);
            setVisible(false);
          }
          catch (NumberFormatException ne){
              JOptionPane.showMessageDialog(this, "Please enter valid year and price");
          }
        }
        else if (e.getSource() == cancelBtn)
        {
             status = false;
             setVisible(false);
            
        }
    }

 	
       
	

	
// return true if Car object is successfully added
	// false otherwise
	public boolean getStatus()
	{
            return (status);
        }
}
