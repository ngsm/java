/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lecture6;

/**The following program reads in two strings, 
 * converts them to integers and displays the result of integer division:
 * 
 * @author ngsm
 *
 */
import java.util.*;
public class ExceptionT1
{
	public static void main(String[] args)
	{
		Scanner sc = new Scanner(System.in);
		System.out.println("This is a program to divide two numbers");
		try
                {
                    System.out.print("Enter first number: ");
                    String numStr = sc.nextLine();
                    int num1 = Integer.parseInt(numStr);

                    System.out.print("Enter second number: ");
                    numStr = sc.nextLine();
                    int num2 = Integer.parseInt(numStr);

                    System.out.println(num1 + "/" + num2 + " = " + (num1/num2));
                }
                catch(Exception e)
                {
                    System.out.println(e.getMessage());
                }   

                
                
	}
}
