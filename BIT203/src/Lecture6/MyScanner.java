/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lecture6;

import java.io.*;
public class MyScanner {

    public static void main(String[] args)
    {
        try
        {
            System.out.print("Enter an integer");
            int val = getInteger();
            System.out.println("You entered " + val);
        }
        catch(NumberFormatException e)
        {
            System.out.println(" Don't you know what an integer is????");
        }
        catch(IOException io)
        {
            System.out.println(io.getMessage());
        }
        finally
        {
            System.out.println("THank you");
        }
        }
public static int getInteger() throws IOException
{
   // declare a large byte array
   byte [] buffer = new byte[512]; 
   // characters entered stored in array 
    System.in.read(buffer);  
// make string from byte array
   String s = new String (buffer); 
   // trim string
   s = s.trim(); 
   // converts string to an 'int'
   int num = Integer.parseInt(s); 
   // send back integer value
   return num;

  }
} 
