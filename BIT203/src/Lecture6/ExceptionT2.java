/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Lecture6;

/**The following program creates a FileReader object, which is used 
 * to read character files.
 *
 * @author ngsm
 *
 */ 
import java.io.*;
import java.util.*;
public class ExceptionT2
{
	public static void main(String[] args)  throws FileNotFoundException
	{
		Scanner sc = new Scanner(System.in);
		System.out.print("Please enter file name: ");
		String fileName = sc.nextLine();
		try {
                     FileReader fr = new FileReader(fileName);
                }
                catch(FileNotFoundException e)
                {
                    System.out.println("File " + fileName + " does not exist" );
                }
                     System.out.println("Done");
	}
}
