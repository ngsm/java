/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

/**
 *
 * @author ngsm
 */
public class TestThread {
    public static void main(String[] args)
    {
        AlphaThread a1 = new AlphaThread('j');
        AlphaThread a2 = new AlphaThread('a');
        AlphaThread a3 = new AlphaThread('v');
        AlphaThread a4 = new AlphaThread('e');
        
        a1.start();
        a2.start();
        a3.start();
        a4.start();
    }
}
