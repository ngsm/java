/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

/**
 *
 * @author ngsm
 */
public class AlphaThread extends Thread{
    
    private char letter;
    
    public AlphaThread(char letter)
    {
        this.letter = letter;
    }
    
    public void run()
    {
        // when this thread runs, it prints the letter 100 times
        for (int i = 0; i < 100; i ++)
            System.out.print(letter);
    }
}
