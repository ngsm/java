/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;
import java.awt.*;
import java.util.Calendar;
import javax.swing.JApplet;

/** DigitalClock.java
 *  Modified version of …
 *  Example from Jia, pp. 150
 */
public class DigitalClock extends JApplet implements Runnable 
{		
	protected Thread clockThread = null;
	protected Font font = new Font("Monospaced", Font.BOLD, 48);
	protected Color color = Color.GREEN;
	// continued

        public void paint(Graphics g)
{
      super.paint(g);

	Calendar calendar = Calendar.getInstance();
	int hour = calendar.get(Calendar.HOUR_OF_DAY);
	int minute = calendar.get(Calendar.MINUTE);
	int second = calendar.get(Calendar.SECOND);
	
	g.setFont(font);
	g.setColor(color);
	g.drawString(hour + ":" + minute/10 + minute%10 + ":" + second/10+ second%10, 10, 60);
}
        public void start() 
	{
		if (clockThread == null)
		{	// create a new thread
			clockThread = new Thread(this);
			clockThread.start();
		}
	}
        
	
	public void stop()
	{	// kill the thread
		clockThread = null;
	}

public void run() 
{
	while (Thread.currentThread() == clockThread)
	{
		repaint();	// indirectly calls paint()
		try 
		{
                    Thread.currentThread().sleep(1000);				      // refresh every second
		}
		catch (InterruptedException e)
		{
		}
	}
}
}
