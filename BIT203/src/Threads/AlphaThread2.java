/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Threads;

/**
 *
 * @author ngsm
 */
public class AlphaThread2 implements Runnable{
    
    private char letter;
    
    public AlphaThread2(char letter)
    {
        this.letter = letter;
    }

    @Override
    public void run() {
        for (int i = 0; i < 100; i++)
            System.out.print(letter);
    }
    
    
}
