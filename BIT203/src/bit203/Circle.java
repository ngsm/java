/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit203;

/**
 * Circle class For Tutorial 1 Q 3
 * Added as a subclass for Lecture 3
 * @author ngsm
 */
public class Circle extends Shape implements Cloneable{
    
    private double radius;
    private Point centre; 

    private final static double PI = 3.14159;
    
    public Circle() {
        centre = new Point();
        radius = 0.0;
    }

    public Circle(double radius)
    {
        this.radius = radius;
    }
    public Circle(double radius, Point centre) {
        this.radius = radius;
        this.centre = centre;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public Point getCentre() {
        return centre;
    }

    public void setCentre(Point centre) {
        this.centre = centre;
    }
    
    public boolean isInside(Point aPoint)
    {
        if (aPoint.distanceTo(centre)< radius)
            return true;
        else
            return false;
    }

    @Override
    public double getArea() {
        return PI * radius * radius;
    }
    public Object clone()
 {
 	try
		{
			return super.clone();
		}
		catch (CloneNotSupportedException e)
		{
			return null;
		}
	}

   public boolean equals(Object c)
    {
        return radius == ((Circle) c).radius;
    }

    
}
