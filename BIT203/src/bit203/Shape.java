/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit203;

/**
 *
 * @author ngsm
 */
public abstract class Shape {
    
    private String name;
    
   abstract public  double getArea();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    
    final  public boolean lessThan(Shape s)
    {
        return this.getArea() < s.getArea();
    }
}
