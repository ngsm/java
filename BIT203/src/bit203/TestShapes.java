package bit203;
import java.util.*;
/**
 * Testing the shapes
 *
 * @author ngsm
 */
public class TestShapes {
    
    public static void main(String[] args)
    {
        // create an array to store 5 shapes
        
        Shape[] myShapes = new Shape[5];
        Scanner sc = new Scanner(System.in);
        Random r = new Random();
        
        // let the user create their choice of shape, 
        // random sizes
        for (int i = 0; i < myShapes.length; i++)
        {
            System.out.println(" (c)ircle or (t)riangle?");
            char ans = sc.next().charAt(0);
            if (ans == 'c')
                myShapes[i] = new Circle(r.nextInt(10));
            else
                myShapes[i] = new Triangle(r.nextInt(10),r.nextInt(10));
        }
        
        // now print the shape details and total area
        double totalArea = 0.0;
        for (int i = 0; i < myShapes.length; i++)
        {
            Shape s = myShapes[i];
            if (s instanceof Circle)
            {
                // downcast so that can use the circle methods,
                // if it is a circle
                Circle c = (Circle) s;
                System.out.println("A circle of radius " +c.getRadius());
            }
            else // use triangle methods
                System.out.println("A triangle with base" + ((Triangle) s).getBase());
            System.out.println("Area is " + s.getArea());
            totalArea += s.getArea();
        }
        System.out.println("Total area of all shapes is "+ totalArea);
     }
}
