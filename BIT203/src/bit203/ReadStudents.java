/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit203;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 *
 * @author ngsm
 */
public class ReadStudents {
    // Reading from the file into the arraylist
      static Scanner sc = new Scanner(System.in);
      static String[] studentNames;
    public static void main(String[] args)
    {
        
        studentNames = new String[50];
        readStudentsFromFile();
        int number = 0;
         for (int i=0; i < studentNames.length; i++)
            if (studentNames[i]!=null)
                number ++;
         System.out.println(number);
        
        String next = "y";
        while (next.equals("y"))
        { 
            int ranNum = (int) (Math.random() * 40 + 1);
            String foundS = studentNames[ranNum];
            if (foundS!=null)
            {
                System.out.println(foundS);
                 studentNames[ranNum] = null;
                System.out.println("Another name?");
                next = sc.next();
            
            }
         }
        
    }
         
public static void readStudentsFromFile()
{

	// get filename
	System.out.print("Enter file name: ");
	String fileName = sc.nextLine();

	// declare file input stream using FileReader
	FileReader fr = null;
       // declare reader data stream – let’s use BufferedReader
	BufferedReader br = null;
	
	
  	try
	{
		// create file stream for the filename required				 	fr = new ????????(????????);
		fr = new FileReader(fileName);
 		// wrap the data stream in file stream
		br = new BufferedReader(fr);

		// read a line from the file
 		String line = br.readLine();
		while (line != null) // null indicates end of file
		{
			// separate the line into the name and the marks 
			StringTokenizer st = new StringTokenizer(line, ",");
			int number = Integer.parseInt(st.nextToken());
                        String name = st.nextToken();
			
			
			// add the student to the array
			studentNames[number] = name;

			// read another line from the file
			line = br.readLine();
 		} // end of while
	}
	catch (FileNotFoundException fnfe)
	{
            System.out.println("File Not Found");
        }
        catch (IOException ioe)
        {
            System.out.println("IO Exception found");
        }

}
}
