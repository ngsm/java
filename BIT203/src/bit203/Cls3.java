/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit203;
import java.util.*;

/**
 *
 * @author ngsm
 */
public class Cls3 extends packA.Cls1{
    
   
   public static void main(String[] args)
   {
       
       ArrayList<Circle> library = new ArrayList<Circle>();
       Circle c1 = new Circle(5);
       Circle c2 = (Circle) c1.clone();
       library.add(c1);
       System.out.println(library.contains(c2));
   }
}
