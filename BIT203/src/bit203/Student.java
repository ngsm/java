/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit203;

/**
 * Employee class 
 * @author ngsm
 */
public class Student extends Person {
    
    private String studentID;
    
    public Student()
    {
        super();
        studentID = "not set";
    }
    public Student(String name, String phoneNumber, String studentID)
    {
        super(name, phoneNumber);
        this.studentID = studentID;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    @Override
    public String toString() {
        return  super.toString() + " is a Student{" + "studentID=" + studentID + '}';
    }
    
    
}
