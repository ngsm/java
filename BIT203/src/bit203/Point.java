/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit203;

/**
 * Point class for Tutorial 1 Q 3
 * @author ngsm
 */
public class Point {
    
    private double xCoord;
    private double yCoord;
    
    public Point()
    {
        xCoord = 0.0;
        yCoord = 0.0;
        
    }
    
    public Point(double x, double y)
    {
        this.xCoord = x;
        this.yCoord = y;
    }
    
    public double getX()
    {
        return xCoord;
    }
    
    public void setX(double x)
    {
        xCoord = x;
    }
    
     public double getY()
    {
        return yCoord;
    }
    
    public void setY(double y)
    {
        yCoord = y;
    }
    
    public String toString()
    {
        return "(" + xCoord + ", " + yCoord + ")";
    }
    
    public double distanceTo(Point anotherPoint)
    {
        // distance is square root of 
        // (x2 - x1)^2 + (y2 - y1)^2
        
        double xDist = this.xCoord - anotherPoint.xCoord;
        double yDist = this.yCoord - anotherPoint.yCoord;
        return Math.sqrt(xDist*xDist + yDist*yDist);
    }
    
}
