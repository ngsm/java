/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bit203;

/**
 *
 * @author ngsm
 */
 public abstract class Person {
    
    private String name;
    private String phoneNumber;

    public Person()
    {
       this("unknown", "unknown");
    }
    
    public Person(String name, String phoneNumber) {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return "name=" + name + ", phoneNumber=" + phoneNumber;
    }
    
    
    
}
