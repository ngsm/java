/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.*;

/**
 *
 * @author ngsm
 */
public class Order implements Serializable, Comparable<Order>{
   
    private int orderNo;
    
    private LocalDateTime orderTime;
    private String status;
    private int numPax;
    private Table theTable;
    private ArrayList<OrderItem> orderedItems;
    private static int nextNo = 500;

    public Order(Table theTable, int numPax) {
        this.orderNo = nextNo++;
        this.numPax = numPax;
        this.theTable = theTable;
        this.orderTime = LocalDateTime.now();
        this.status = "new";
        this.orderedItems = new ArrayList<>();
       
    }

    public OrderItem addItem(MenuItem mi, int quantity, String request)
    {
        OrderItem oi = new OrderItem(mi, this, quantity, request);
        if (orderedItems.contains(oi))
            return null;
        orderedItems.add(oi);
        return oi;
    }
    public int getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(int orderNo) {
        this.orderNo = orderNo;
    }

    public static void setNextNo(int next)
    {
        nextNo = next;
    }
            
    public LocalDateTime getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(LocalDateTime orderTime) {
        this.orderTime = orderTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumPax() {
        return numPax;
    }

    public void setNumPax(int numPax) {
        this.numPax = numPax;
    }

   

        public Table getTable() {
        return theTable;
    }

    public void setTable(Table theTable) {
        this.theTable = theTable;
    }

    public double getTotal()
    {
        double total = 0.0;
        for (OrderItem oi:orderedItems)
        {
            total += oi.getTotal();
        }
        return total;
    }
    
    public boolean removeItem(OrderItem unwanted)
    {
     	 return orderedItems.remove(unwanted);
    }

    @Override
    public String toString() {
        return "orderNo=" + orderNo + ", orderTime=" + orderTime + ", status=" + status + ", numPax=" + numPax + ", tableNo=" + theTable.getTableNo();
    }
    
    public ArrayList<OrderItem> getOrderItems()
    {
        return orderedItems;
    }

    @Override
    public int compareTo(Order od) {
         return this.orderNo - od.orderNo;
    }
}
