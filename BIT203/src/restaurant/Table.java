/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;
import java.io.Serializable;
import java.util.*;
/**
 *
 * @author ngsm
 */
public class Table implements Serializable {
 
    
    private int tableNo;
    private int seatingCapacity;
    private String currentStatus;
    private int xPos;
    private int yPos;
    private ArrayList<Order> tableOrders;
    
    
    public Table(int tableNo, int seatingCapacity, int xPos, int yPos) {
        this.tableNo = tableNo;
        this.seatingCapacity = seatingCapacity;
        this.xPos = xPos;
        this.yPos = yPos;
        this.currentStatus="available";
        tableOrders = new ArrayList<>();
    }

    public ArrayList<Order> getOrders()
    {
        return tableOrders;
    }
    
    public int getTableNo() {
        return tableNo;
    }

    public int getSeatingCapacity() {
        return seatingCapacity;
    }

    public String getCurrentStatus() {
        return currentStatus;
    }

    public void setCurrentStatus(String currentStatus) {
        this.currentStatus = currentStatus;
    }

    public int getxPos() {
        return xPos;
    }

    public int getyPos() {
        return yPos;
    }

    @Override
    public String toString() {
        return "Table{" + "tableNo=" + tableNo + ", seatingCapacity=" + seatingCapacity + ", currentStatus=" + currentStatus + ", xPos=" + xPos + ", yPos=" + yPos + '}';
    }
    
    public Order addOrderToTable(int numPax)
    {
        Order newOrder = new Order(this, numPax);
        tableOrders.add(newOrder);
        setCurrentStatus("occupied");
        return newOrder;

    }
    
    
            
}
