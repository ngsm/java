/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ngsm
 */
public class OrderTableModel extends AbstractTableModel{
    
    private static final String[] colHeader = {"Order No", "Table No", "Order Date", "Order Time", "Status"};
    private ArrayList<Order> orders;
    
    // constructor to set the model to an array passed in
    public OrderTableModel(ArrayList<Order> orders)
    {
        setOrders(orders);
        
    }
    

    @Override
    public int getRowCount() {
        return orders.size();
    }

    @Override
    public int getColumnCount() {
        return colHeader.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Order theOrder = (Order) orders.get(rowIndex);
        switch(columnIndex)
        {
            case 0: return new Integer(theOrder.getOrderNo());
            case 1: return new Integer(theOrder.getTable().getTableNo());
            case 2: return theOrder.getOrderTime().toLocalDate();
            case 3: return theOrder.getOrderTime().getHour() + ":" + theOrder.getOrderTime().getMinute();
            case 4: return theOrder.getStatus();
            default: return "";
                
        }
    }
    
    public String getColumnName(int column)
    {
        return colHeader[column];
    }
    
    public Order getElementAt(int index)
    {
        Order theOrder =(Order) orders.get(index);
        return theOrder;
    }
    
    public void setOrders(ArrayList<Order> orders)
    {
        this.orders = orders;
    }
}
