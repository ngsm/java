/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.io.Serializable;

/**
 *
 * @author ngsm
 */
public class MenuItem implements Serializable{
    
    private int itemNo;
    private String itemDescription;
    private double itemPrice;
    private static int nextNo = 100;

    public MenuItem(String itemDescription, double itemPrice) {
        this.itemNo = nextNo++;
        this.itemDescription = itemDescription;
        this.itemPrice = itemPrice;
    }

    public static int getNextNo()
    {
        return nextNo;
        
    }
    
    public static void setNextNo(int next)
    {
        nextNo = next;
    }
    public int getItemNo() {
        return itemNo;
    }

    public void setItemNo(int itemNo) {
        this.itemNo = itemNo;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    @Override
    public String toString() {
        return "MenuItem{" + "itemNo=" + itemNo + ", itemDescription=" + itemDescription + ", itemPrice=" + itemPrice + '}';
    }
    
    
    
}
