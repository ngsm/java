/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author ngsm
 */
public class OrderItemTableModel extends AbstractTableModel{
    
    private static final String[] colHeader = {"Menu Item", "Quantity", "Special Request", "Status"};
    private ArrayList<OrderItem> orderItems;
    
    
    // constructor to set the model to an array passed in
    public OrderItemTableModel()
    {
        setOrderItems(new ArrayList<OrderItem>());
        
    }
    

    @Override
    public int getRowCount() {
        return orderItems.size();
    }

    @Override
    public int getColumnCount() {
        return colHeader.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        OrderItem theOrderItem = (OrderItem) orderItems.get(rowIndex);
        MenuItem theMenuItem = theOrderItem.getTheMenuItem();
        switch(columnIndex)
        {
            
            case 0: return theMenuItem.getItemNo() + " " + theMenuItem.getItemDescription();
            case 1: return new Integer(theOrderItem.getQuantity());
            case 2: return theOrderItem.getSpecialRequest();
            case 3: return theOrderItem.getStatus();
             default: return "";
                
        }
    }
    
    public String getColumnName(int column)
    {
        return colHeader[column];
    }
    
    public OrderItem getElementAt(int index)
    {
        OrderItem theOrderItem =(OrderItem) orderItems.get(index);
        return theOrderItem;
    }
    
    public void setOrderItems(ArrayList<OrderItem> orderItems)
    {
        this.orderItems = orderItems;
    }
}
