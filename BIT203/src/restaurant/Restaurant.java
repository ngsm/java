/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package restaurant;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author ngsm
 */
public class Restaurant implements Serializable {
    
    private ArrayList<MenuItem> menuItems;
    private ArrayList<Table> tables;
    
    public Restaurant()
    {
        menuItems = new ArrayList<>();
        tables = new ArrayList<>();
        setupTables();
        
    }   
    
    public ArrayList<Order> allOrders()
    {
        ArrayList<Order> orderList = new ArrayList<>();
        for (Table t:tables)
        {
            orderList.addAll(t.getOrders());
        }
        return orderList;
    }
    
    
    public MenuItem findMenuItem(int itemNo)
    {
        for (MenuItem mi: menuItems)
            if (mi.getItemNo() == itemNo)
                return mi;
        return null;
    }
    
    public void addMenuItem(String description, double price)
    {
        MenuItem mi = new MenuItem(description, price);
        menuItems.add(mi);
        
                
    }
    
    public void addTable(int tableNo, int seatingCapacity, int xPos, int yPos)
    {
        Table t = new Table(tableNo, seatingCapacity, xPos, yPos);
        tables.add(t);
    }
    
    public void updateItem(MenuItem theItem, String description, double price)
    {
        theItem.setItemDescription(description);
        theItem.setItemPrice(price);
    }
    
    public ArrayList<Table> getTables()
    {
        return tables;
    }
    
    public Table getTable(int tableNo)
    {
        for (Table t:tables)
        {
            if (t.getTableNo()==tableNo)
                return t;
        }
        return null;
    }
    public Order createNewOrder(int tableNo, int numPax)
    {
        Table theTable = getTable(tableNo);
        if (theTable==null)
            return null;
        return theTable.addOrderToTable(numPax);
        
    }
    public void setupTables()
    {
        // create 16 tables in positions x = 10, 20, 30, 40, y = 10, 20, 30, 40
        int tableNo = 1;
        for (int y = 10; y <=250; y+=70 )
        {
            for (int x = 10; x<=250; x+=70)
            {
                int cap = (int) ((Math.random() * 6) + 2); // generate random seating
                addTable(tableNo++, cap, x, y);
  
            }
        }
        
    }
    
    public void updateSequences()
    {
        ArrayList<Order> orders = allOrders();
        // Must sort all orders because they may be based on table
        Collections.sort(orders);
        
        int lastIndex = orders.size() - 1;
        if (lastIndex >=0)
            Order.setNextNo(orders.get(lastIndex).getOrderNo()+1);
        else 
            Order.setNextNo(500);
        
        // Menu items should already be in order
        lastIndex = menuItems.size() - 1;
        if (lastIndex >=0)
            MenuItem.setNextNo(menuItems.get(lastIndex).getItemNo()+1);
        else
            MenuItem.setNextNo(100);
    }
}
