/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbconnecting;

/**
 *
 * @author ngsm
 */
public class Student {
    private String name;
    private String studentID;
    private double Score;

    public Student(String studentID, String name, double Score) {
        this.name = name;
        this.studentID = studentID;
        this.Score = Score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudentID() {
        return studentID;
    }

    public void setStudentID(String studentID) {
        this.studentID = studentID;
    }

    public double getScore() {
        return Score;
    }

    public void setScore(double Score) {
        this.Score = Score;
    }

    @Override
    public String toString() {
        return "Student{" + "name=" + name + ", studentID=" + studentID + ", Score=" + Score + '}';
    }
    
}
