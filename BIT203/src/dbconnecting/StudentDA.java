/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbconnecting;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author ngsm
 */
public class StudentDA {
    
    private Connection conn;
    
    //method to save student
    public boolean save(Student s)
    {
        Statement stmt = null;
       try {
            conn = MySQLDBConnection.getConnection(); 
            stmt = conn.createStatement();
            String sql = "INSERT INTO student VALUES ('"
            + s.getStudentID() +"','" +s.getName() 
         + "'," + s.getScore() + ")";

            stmt.executeUpdate(sql);
            stmt.close();
            conn.close();
            return true;
}catch(SQLException se){
      //Handle errors for JDBC
   		se.printStackTrace();
   		return false;
   	}finally{		// close resources
 		try{
      			if (stmt!=null) stmt.close();
          		if (conn!=null) conn.close();
      		}catch(SQLException se){
         		se.printStackTrace();
         		return false;
      		}
   	}//end finally
	

           
    }
    
    public Student getStudent(String studentID)
    {
        Statement stmt = null;
        Student s = null;
        try {
            conn = MySQLDBConnection.getConnection();
            stmt = conn.createStatement();
            String sql = "SELECT studentName, score FROM student WHERE studentID = '" + studentID + "'  ";

            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                //Retrieve by column name
                String studentName = rs.getString("studentName");
                double score = rs.getDouble("score");
                s = new Student(studentID, studentName, score);
            }
            rs.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException se) {
                se.printStackTrace();
            }//end try
        }//end finally
        return s;	// return the Student object
    }


}
