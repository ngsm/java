/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbconnecting;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**
 *
 * @author ngsm
 */
public class MySQLDBConnection {
    
    private static MySQLDBConnection instance = new MySQLDBConnection();
    
    private static String DB_URL = "jdbc:mysql://localhost/university";
    private static String USER = "root";
    private static String PASS = "root";
    
    // Add this method to create and return a Connection object   
   private Connection createConnection()   {
 	Connection conn = null;
	try 	{
 	  System.out.println("Connecting to database ...");
	  conn = DriverManager.getConnection(DB_URL, USER, PASS);
	}catch(SQLException se){
      //Handle errors for JDBC
      	   se.printStackTrace();
	}
      return conn;
    }
   
   public static Connection getConnection()
   {
       return instance.createConnection();
   }

}
