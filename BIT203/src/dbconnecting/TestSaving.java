/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dbconnecting;

/**
 *
 * @author ngsm
 */
public class TestSaving {
    
    public static void main(String[] args)
    {
        Student s = new Student("A11", "Mary", 99.9);
        StudentDA sda = new StudentDA();
        sda.save(s);
        
        Student anotherStudent = sda.getStudent("B1400");
        System.out.println("Another student is " + anotherStudent.toString() );
    }
}
